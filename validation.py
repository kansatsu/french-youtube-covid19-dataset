import pandas as pd
import numpy as np
from scipy.interpolate import make_interp_spline
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import seaborn as sn
import re
import locale
import sys
import os
locale.setlocale(locale.LC_ALL, 'en_US')

from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle

db_files = ['YTVideos.csv', 'YTChannel_stats.csv', 'YTChannel_recommendations.csv', 'YTVideo_captions.csv', 'YTChannels.csv', 'YTVideo_stats.csv', 'YTVideo_recommendations.csv']

def spliner(x,y,n,k):
    xnew = np.linspace(x.min(),x.max(),n)
    spl = make_interp_spline(x, y, k=k)
    ynew = spl(xnew)
    return xnew,ynew;

def main(folder):
    
    print('\n\nData Descriptor Validation (Scientific Data):\nThe French YouTube Media and Recommendation System During the Onset of the COVID-19 Pandemic\n')
    
    # folder = os.path.abspath(folder)
    folder = '/Users/pedroramaciotti/Proyectos/Kansatsu/NSDArticle'#DELETE
    print('   * Exploring contents of folder %s ...'%folder)

    # Checking files
    folder_files = [f for f in os.listdir(folder) if f.endswith('.csv')]
    available_files = np.intersect1d(folder_files,db_files)
    if available_files.size!=7:
        raise ValueError('Some files are missing.')
        
    # Loading files
    print('   * Loading dataset files ...')
    table={}
    for file in available_files:
        table[file] = pd.read_csv(folder+'/'+file)
    sb = pd.read_csv('top250FrenchYoutubers.csv')
    
    # Exporting description of data records
    print('   * Printing data records ...')
    with open('data_descriptor.txt', 'w') as f:
        for file,df in table.items():
            print('\n\n%s (%d entries)\n'%(file,df.shape[0]),file=f)  
            print('Columns:',file=f)
            for c in df.columns:
                print('%s: type=%s'%(c,str(df[c].dtypes)),file=f)
            
    # Channels per country
    print('   * Channels per country ...')
    channel_countries = table['YTChannels.csv']['country'].value_counts().sort_values(ascending=False)
    fig = plt.figure(figsize=(20,3))# width, height inches
    ax = fig.add_subplot(1,1,1)
    ax.bar(np.arange(channel_countries.size),channel_countries.values)
    ax.set_xticks(np.arange(channel_countries.size))
    ax.set_xticklabels(channel_countries.index,fontsize=5,rotation=45,ha='right')
    ax.set_xlabel('country'),ax.set_ylabel('channels'),ax.set_yscale('log')
    plt.tight_layout()
    plt.savefig('channel_ountries.pdf') 
    plt.close()
    
    # Comparing with social blade and blog du modérateur
    print('   * Comparing channels with third party data ...')
    table['YTChannels.csv']['isin_SB']=table['YTChannels.csv']['channel_name'].isin(sb.username)

    social_blade_hits = table['YTChannels.csv']['isin_SB'].sum()
    social_blade_hits_pc = 100*social_blade_hits/sb.shape[0]

    FR_channel_ids = table['YTChannels.csv'].loc[table['YTChannels.csv']['country']=='FR','channel_id']
    channels_geq1M=(table['YTChannel_stats.csv'].loc[table['YTChannel_stats.csv']['channel_id'].isin(FR_channel_ids),'subscribers']>1e6).sum()


    intersection_names = table['YTChannels.csv'].loc[table['YTChannels.csv']['channel_name'].isin(sb.username),'channel_name']
    channels_notin_kansatsu = sb.loc[~sb['username'].isin(intersection_names)]
    
    with open('validation.txt', 'w') as f:
        print('Channels with more than 1M subscribers: %d'%channels_geq1M,file=f)
        print('Detected channels among the top 250 French channels (Social Blade): %d (%.2f%%)'%(social_blade_hits,social_blade_hits_pc),file=f)
        print('List of undetected top 250 channels (Social Blade): ',file=f)
        for idx,row in channels_notin_kansatsu.iterrows():
            print(' Name: %s. Subscribers: %s. Videos: %d'%(row['username'],row['subscribers'],row['videos']),file=f)

    # Counting videos with keywords for usage notes
    print('   * Counting word-pattern examples for usage ...')
    patterns = [r'crise',r'virus',r'confinement',r'vaccin',r'masque',r'd[e|é]confinement']

    with open('caption_keywords.txt', 'w') as f:
        for pattern in patterns:
            table['YTVideo_captions.csv']['pattern_%s'%pattern] = table['YTVideo_captions.csv']['caption'].apply(lambda c: True if re.search(pattern,c) else False)
            count=table['YTVideo_captions.csv']['pattern_%s'%pattern].sum()
            print('pattern %s: %d (%.2f%%)'%(pattern,count,100.0*count/table['YTVideo_captions.csv'].shape[0]),file=f)
    
    # Sample of video recommendations time-series
    print('   * Sample figure of recommendation time-series ...')
    df_edges = table['YTVideo_recommendations.csv'].copy(deep=True)
    df_edges['datetime'] = pd.to_datetime(table['YTVideo_recommendations.csv']['acquisition_datetime']).dt.tz_localize(None)
    df_edges = df_edges[(df_edges['datetime']<=pd.to_datetime('2020-06-12'))&(df_edges['datetime']>=pd.to_datetime('2020-06-04'))]
        # Resampling
    df_edges['recs'] = 1
    df_edges_resampled = df_edges[['datetime','recs']].resample('1440min', on='datetime').sum()
        # Splining
    xnew,ynew = spliner(np.arange(df_edges_resampled.shape[0]),df_edges_resampled['recs'].values,n=100,k=2)
        # the figure
    fig = plt.figure(figsize=(6, 4))
    ax=fig.add_subplot(1,1,1)
    ax.plot(xnew,ynew)
    ax.set_xlim((0,df_edges_resampled.shape[0]-1))
    ax.set_ylim((0,375e3))
    ax.set_xticks(np.arange(df_edges_resampled.shape[0]))
    ax.set_xticklabels(df_edges_resampled.index.strftime('%b-%d').values)
    ax.axvspan(1, 2, alpha=0.5, color='green')
    ax.set_ylabel('Daily video-to-video recommendations',fontsize=12)
    ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: '{:,.0f}'.format(x/1000) + 'K'))
    plt.tight_layout()
    plt.savefig('zoom_recommendation_timeseries.pdf')
    plt.clf()
    plt.close()
    
    # the end
    print('\nThe end\n')


if __name__ == "__main__":
    
    if len(sys.argv)!=2:
        raise ValueError('Correct use requires path to dataset : python validation.py <FOLDER_PATH>')
    
    main(sys.argv[1])