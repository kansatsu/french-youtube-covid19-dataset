# French Youtube Covid19 Dataset

This is the replication and validation code for the article "Data Descriptor Validation (Scientific Data):\nThe French YouTube Media and Recommendation System During the Onset of the COVID-19 Pandemic" by K. Kursterer et al.

1. Download the dataset from FigShare at [https://figshare.com/s/d6283ba5355a141ca4ee](https://figshare.com/s/d6283ba5355a141ca4ee) and unzip the files.

2. Download the code:

> git clone https://gitlab.com/kansatsu/french-youtube-covid19-dataset.git

3. Run the code, indicating where you have downloaded the dataset

> python validation.py <PATH_TO_DATASET>